#include <iostream>
#include <sstream>
#include <cmath>
#include <algorithm>

using namespace std;
/**
* 此题有问题，
* 当前代码第一个样例结果输出为 398
* 但是，AC了。
* 猜测是 pow() 函数的原因。
**/

char intToChar(int ori)
{
    stringstream stream;
    stream << ori;
    char re;
    stream >> re;
    return re;
}

int getPa(string a, int Da)
{
    int Pa = 0;
    reverse(a.begin(),a.end());
    int index = 0;
    for (int i = 0; i < a.length(); i++)
    {
        if (a[i] == intToChar(Da))
        {
            Pa = Pa + Da*(pow(10,index));
            index++;
        }
    }
    return Pa;
}

int main()
{
    string a,b;
    int Da, Db;
    cin >> a >> Da >> b >> Db;

    cout << getPa(a,Da)+getPa(b,Db);

    return 0;
}
