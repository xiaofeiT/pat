#include <iostream>
#include <iomanip>
#include <stdio.h>

using namespace std;

int main()
{
    int n;
    cin >> n;

    int a1 = 0,a2 = 0,a3 = 0,a4Num = 0,a5 = 0;
    bool hasA2 = false; // 因为有可能进行相加减后结果为0，所以增加一个变量来记录是否模5余1的数
    float a4 = 0,a4Sum = 0;
    int a2Flag = 0; // 0->-,1->+

    for (int i = 0; i < n; i++)
    {
        int number;
        cin >> number;

        //a1
        if (number % 10 == 0) a1+= number;
        //a2
        if (number % 5 == 1)
        {
            hasA2 = true;
            if(a2Flag)
            {
                a2 -= number;
                a2Flag = 0;
            }
            else
            {
                a2 += number;
                a2Flag = 1;
            }
        }
        //a3
        if (number % 5 == 2) a3++;
        //a4
        if (number % 5 == 3)
        {
            a4Sum += number;
            a4Num ++;
        }
        //a5
        if (number % 5 == 4 && number > a5) a5 = number;
    }

    if (a1 == 0) cout << "N ";
    else cout << a1 << " ";
    if ( !hasA2 ) cout << "N ";
    else  cout << a2 << " ";
    if (a3 == 0) cout << "N ";
    else cout << a3 << " ";
    if (a4Num == 0) cout << "N ";
    else cout << setiosflags(ios::fixed) << setprecision(1) << (a4Sum)/a4Num << " ";
    //else printf("%.1f", a4Sum/a4Num);
    if (a5 == 0) cout << "N";
    else cout << a5;

    return 0;
}
