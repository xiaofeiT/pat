#include <iostream>
#include <cstring>

using namespace std;

int main()
{
    string str;
    getline(cin,str);

    string strs[80];

    int index = 0;

    for (int i = 0; i < str.length(); i++)
    {
        if(str[i] == ' ')
        {
            index++;
        }
        else
        {
            strs[index] += str[i];
        }
    }

    for (int i = index; i >= 0; i--)
    {
        cout << strs[i];
        if ( i != 0) cout << " ";
    }

    return 0;
}
