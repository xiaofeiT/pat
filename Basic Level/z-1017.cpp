#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

int charToInt(char ori)
{
    stringstream stream;
    int re;
    stream << ori;
    stream >> re;
    return re;
}

int main()
{
    string a;
    int b;
    cin >> a >> b;

    vector<int> q;
    int r = 0;

    int length = a.length();
    int temp = charToInt(a[0])*10 + charToInt(a[1]);

    if (length <= 1)
    {
        q.push_back(charToInt(a[0])/b);
        r = (charToInt(a[0])%b);
    }

    for (int i = 2; i <= length; i++)
    {
        int shang = temp / b;
        q.push_back(shang);
        r = temp % b;
        // cout << "temp/b:" << temp << "/" << b << "=" << shang << " ... " << r << endl;
        temp = r * 10 + charToInt(a[i]);
        // cout << temp << endl;
    }

    for (int i = 0; i < q.size(); i++)
    {
        cout << q[i];
    }
    cout << " " << r;

    return 0;
}
