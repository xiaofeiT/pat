#include <iostream>

using namespace std;

int main()
{
    int a,b,isFirst = 1;

    while(cin >> a >> b)
    {
        if (b != 0)
        {
            if (!isFirst) cout << " ";
            cout << a*b << " " << b-1;
            isFirst = 0;
        }
    }
    if (!isFirst) cout << "0 0";

    return 0;
}
