#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Student
{
    int id;
    int de;
    int cai;
    int zong; //总分
};

bool comparsion(Student a, Student b)
{
    if (a.zong == b.zong)
    {
        if( a.de == b.de ) return a.id < b.id; //准考证号的升序
        else return a.de > b.de; //降序
    }
    else
    {
        return a.zong > b.zong;
    }
}

void print(vector<Student> a)
{
    int length = a.size();
    for (int i = 0; i < length; i++)
    {
        cout << a[i].id << " " << a[i].de << " " << a[i].cai << endl;
    }
}

int main()
{
    int n,l,h;
    cin >> n >> l >> h;

    vector<Student> caidequanjian; //德分和才分均不低于此线
    vector<Student> deshengcai; //才分不到但德分到线的
    vector<Student> caidejianbei;   //德才分均低于 H，但是德分不低于才分
    vector<Student> other;


    for (int i = 0; i < n; i++)
    {
        Student student;
        cin >> student.id;
        cin >> student.de;
        cin >> student.cai;
        student.zong = student.de + student.cai;
        if ( student.de >= l && student.cai >= l)
        {
            if (student.de >= h && student.cai >= h) caidequanjian.push_back(student);
            else if (student.de >= h && student.cai < h) deshengcai.push_back(student);
            else if (student.de < h && student.cai <h && student.de >= student.cai) caidejianbei.push_back(student);
            else other.push_back(student);
        }
    }

    sort(caidequanjian.begin(),caidequanjian.end(),comparsion);
    sort(deshengcai.begin(), deshengcai.end(), comparsion);
    sort(caidejianbei.begin(), caidejianbei.end(), comparsion);
    sort(other.begin(), other.end(), comparsion);

    cout << (caidequanjian.size()+deshengcai.size()+caidejianbei.size()+other.size()) << endl;
    print(caidequanjian);
    print(deshengcai);
    print(caidejianbei);
    print(other);

    return 0;
}
