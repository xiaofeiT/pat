#include <iostream>
#include <string>

using namespace std;

int main()
{
    string days[7] = {"MON","TUE","WED","THU","FRI","SAT","SUN"};
    bool hasConfirmDay = false;

    string str1,str2,str3,str4;
    cin >> str1 >> str2 >> str3 >> str4;

    for (int i = 0; i < str1.length(); i++)
    {
        if( !hasConfirmDay && str1[i]==str2[i] && str1[i]>='A' && str1[i]<='G' )
        {
            cout << days[str1[i]-'A'] << " ";
            hasConfirmDay = true;
        }
        else if( hasConfirmDay && str1[i]==str2[i] )
        {
            if (str1[i] >= 'A' && str1[i] <= 'N')
            {
                cout << (str1[i]-'A'+10) << ":";
                break;
            }
            else if (str1[i] >= '0' && str1[i] <= '9' )
            {
                cout << "0" << str1[i] << ":";
                break;
            }
        }
    }

    for (int i = 0; i < str3.length(); i++)
    {
        if (str3[i]==str4[i] && ((str3[i] >= 'a' && str3[i] <='z') || (str3[i]>='A' && str3[i]<='Z')))
        {
            if(i<10) cout << "0" << i;
            else cout << i;
        }
    }

    return 0;
}
