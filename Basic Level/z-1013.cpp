#include <iostream>
#include <vector>

using namespace std;

bool isPrime(int ori)
{
    for(int i = 2; i*i <= ori; i++)
    {
        if(ori % i == 0) return false;
    }
    return true;
};

int main()
{
    int m = 0, n= 0;
    cin >> m >> n;

    vector<int> vec;
    int primeOrder = 0;
    int temp = 2;

    while( primeOrder < n)
    {
        if (isPrime(temp))
        {
            primeOrder++;
            if (primeOrder >= m)
            {
                vec.push_back(temp);
            }
        }
        temp ++;
    }

    for (int i = 0; i < vec.size(); i++)
    {
        if (i%10 == 0 && i!=0) cout << endl;
        else if (i% 10 != 0) cout << " ";

        cout << vec[i];
    }
    return 0;
}
